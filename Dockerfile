#Docker ps shows us what is running:
#https://stackify.com/guide-docker-java/
#spring-boot-app:latest nome:tag
# docker build docker -t spring-boot-app:latest
#docker run -d  -p 8761:8761 spring-boot-app:latest
#http://192.168.99.100:8761/

# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine
# copy WAR into image
COPY eureka-server-0.0.1-SNAPSHOT.jar /eureka-server.jar
# run application with this command line 
CMD ["/usr/bin/java", "-jar", "-Dspring.profiles.active=default", "/eureka-server.jar"]