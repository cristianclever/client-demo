# README #

Projeto client-demo


Urls do Projeto:

* http://localhost:8080/client-api/actuator/health
* http://localhost:8080/client-api/actuator/info
* http://localhost:8080/client-api/swagger-ui.html


# LISTA DE MELHORIAS #
* IMPLEMENTACAO DE CACHE NA CHAMADA A API DOS CORREIOS REMOVENDO CHECAGEM EM DB
* ADICAO DE RESILIENCIA A CHAMADA DE API DOS CORREIOS (CIRCUIT BREAKER / BULKHEAD)
* ADICAÇAÕ DE SEGURANÇA UTILIZANDO TOKENS 
* MIGRAÇÃO DA BASE DE DADOS... SQLSERVER AZURE, E POSTERIORMENTE COSMOSDB



#openshift#
http://client-demo-git-cristianclever-dev.apps.sandbox.x8i5.p1.openshiftapps.com/client-api/swagger-ui.html

O plano developer deixa o pod ativo por 8 horas apenas