package com.verx.clientdemo.domain.config;

import javax.sql.DataSource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration

public class DataSourceConfig {

	@Bean
	DataSource createDatasource() {
		HikariConfig hikariConfig = getHikariConfigProperties();
		HikariDataSource ds = new HikariDataSource(hikariConfig);
		return ds;
	}

	@Bean
	@ConfigurationProperties(prefix = "datasource.hikari")
	public HikariConfig getHikariConfigProperties() {
		HikariConfig hikariConfig = new HikariConfig();
		return hikariConfig;
	}

}
