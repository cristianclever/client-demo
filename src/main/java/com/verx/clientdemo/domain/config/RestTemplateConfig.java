package com.verx.clientdemo.domain.config;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfig {

	private final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(RestTemplateConfig.class);

	@Value("${DEFAULT_CONNECT_TIMEOUT:2501}")
	private int defaultConnectTimeout;
	
	@Value("${DEFAULT_READ_TIMEOUT:2502}")
	private int defaultReadTimeout;
	
	
	
	
    private void logRequest(HttpRequest request, byte[] body) throws IOException 
    {
        if (log.isDebugEnabled()) 
        {
            log.debug("===========================request begin================================================");
            log.debug("URI         : {}", request.getURI());
            log.debug("Method      : {}", request.getMethod());
            log.debug("Headers     : {}", request.getHeaders());
            log.debug("Request body: {}", new String(body, "UTF-8"));
            log.debug("==========================request end================================================");
        }
    }
    
    
	
	@Bean
	public RestTemplate restTemplate() {
		SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
		factory.setConnectTimeout(defaultConnectTimeout);
		factory.setReadTimeout(defaultReadTimeout);
		var restTemplate = new RestTemplate(factory);
		return restTemplate;
	}

	@Qualifier("rest-template-with-logger")
	@Bean
	public RestTemplate restTemplateWithLog() {
		SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
		factory.setConnectTimeout(defaultConnectTimeout);
		factory.setReadTimeout(defaultReadTimeout);
		
		var restTemplate = new RestTemplate(factory);
		restTemplate.getInterceptors().add(new ClientHttpRequestInterceptor() {
			
			@Override
			public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)	throws IOException {
				logRequest(request, body);
				
				return execution.execute(request, body);
			}
		});
		
		return restTemplate;
	}
}
