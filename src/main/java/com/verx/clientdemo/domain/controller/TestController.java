package com.verx.clientdemo.domain.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.verx.clientdemo.domain.dto.ClientDTO;
import com.verx.clientdemo.domain.dto.ClientRequestDTO;
import com.verx.clientdemo.domain.dto.PageableClientDTO;
import com.verx.clientdemo.domain.dto.ViacepDTO;
import com.verx.clientdemo.domain.service.PersistenceService;
import com.verx.clientdemo.domain.service.ViaCepService;

@RequestMapping("/v1/api/test")
@RestController
public class TestController {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(TestController.class);

	@Autowired
	private ViaCepService cepService;

	@GetMapping("/cep/{cep}")
	public ResponseEntity<ViacepDTO> getClient(@PathVariable long cep) {
		ViacepDTO   cepDto = cepService.findByCep(cep);
		return ResponseEntity.status(HttpStatus.OK).body(cepDto);
	}


}
