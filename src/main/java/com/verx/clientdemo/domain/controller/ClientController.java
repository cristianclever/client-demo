package com.verx.clientdemo.domain.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.verx.clientdemo.domain.dto.ClientDTO;
import com.verx.clientdemo.domain.dto.ClientRequestDTO;
import com.verx.clientdemo.domain.dto.PageableClientDTO;
import com.verx.clientdemo.domain.service.PersistenceService;

@RequestMapping("/v1/api/clients")
@RestController
public class ClientController {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(ClientController.class);

	@Autowired
	private PersistenceService persistenceService;

	@GetMapping("/{cpf}")
	public ResponseEntity<ClientDTO> getClient(@PathVariable String cpf) {
		ClientDTO result = persistenceService.findByID(cpf);
		return ResponseEntity.status(HttpStatus.OK).body(result);
	}

	@PostMapping("/")
	public ResponseEntity<ClientDTO> newClient(@RequestBody ClientRequestDTO newClientDTO) {
		ClientDTO result = persistenceService.insert(newClientDTO);
		return ResponseEntity.status(result == null ? HttpStatus.NO_CONTENT : HttpStatus.CREATED).body(result);
	}

	@PutMapping("/")
	public ResponseEntity<ClientDTO> replaceClient(@RequestBody ClientRequestDTO newClientDTO) {
		ClientDTO result = null;
		result = persistenceService.update(newClientDTO);
		return ResponseEntity.status(result == null ? HttpStatus.NOT_MODIFIED : HttpStatus.ACCEPTED).body(result);
	}

	@GetMapping("/")
	public ResponseEntity<PageableClientDTO> getAllClients(@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer size) {
		Pageable pageable = Pageable.unpaged();

		if (page != null && size != null) {
			pageable = PageRequest.of(page, size);
		}
		PageableClientDTO retorno = persistenceService.findAll(pageable);
		return ResponseEntity.status(HttpStatus.OK).body(retorno);
	}

	@DeleteMapping("/{cpf}")
	public ResponseEntity<ClientDTO> deleteByCpf(@PathVariable String cpf) {
		persistenceService.deleteByID(cpf);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}

}
