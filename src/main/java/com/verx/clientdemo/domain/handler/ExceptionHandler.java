package com.verx.clientdemo.domain.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.verx.clientdemo.domain.exception.CepNotFoundException;

@RestControllerAdvice
public class ExceptionHandler {

	final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(ExceptionHandler.class);

	@org.springframework.web.bind.annotation.ExceptionHandler(CepNotFoundException.class)
	public ResponseEntity<String> handleException(CepNotFoundException e) {
		log.error("Erro ao obter cep ->", e);
		return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
