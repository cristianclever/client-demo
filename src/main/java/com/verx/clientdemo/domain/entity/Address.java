package com.verx.clientdemo.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TB_ADDRESS")
public class Address implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "POSTAL_CODE", unique = false, nullable = false, length = 8)
	private Long postalCode;

	@Column(unique = false, nullable = false, length = 300)
	private String street;

	//bairro
	@Column(unique = false, nullable = false, length = 250)
	private String district;

	@Column(unique = false, nullable = false, length = 80)
	private String city;

	@Column(unique = false, nullable = false, length = 80)
	private String state;

	public Long getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(Long postalCode) {
		this.postalCode = postalCode;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}



}
