package com.verx.clientdemo.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



	
@Entity
@Table(name = "TB_CLIENT")	
public class Client implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "CPF", unique = true, nullable = false, length = 11)
	private String cpf;
	
	@Column(name = "NAME", unique = false, nullable = false, length = 250)
	private String name;
	
	
	@ManyToOne()
	@JoinColumn(name="POSTAL_CODE" )
	
	private Address address;
	
	


	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

}
