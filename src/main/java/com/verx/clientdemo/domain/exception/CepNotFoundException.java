package com.verx.clientdemo.domain.exception;

public class CepNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public CepNotFoundException(Long cep) {
		super("CEP:" + cep + " not Found" );
	}
	

}
