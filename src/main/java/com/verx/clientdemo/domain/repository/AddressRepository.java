package com.verx.clientdemo.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.verx.clientdemo.domain.entity.Address;
import com.verx.clientdemo.domain.entity.Client;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {

}

