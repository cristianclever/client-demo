package com.verx.clientdemo.domain.util;

import java.text.MessageFormat;

import org.ehcache.event.CacheEvent;
import org.ehcache.event.CacheEventListener;

public class CacheEventLogger implements CacheEventListener<Object, Object>{

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(CacheEventLogger.class);	
	
	@Override
	public void onEvent(CacheEvent<? extends Object, ? extends Object> cacheEvent) {
		
		String result = MessageFormat.format("CacheEvent=> Type[{3}] Key[{0}] oldValue:[{1}] newValue[{2}]", cacheEvent.getKey(),cacheEvent.getOldValue(),cacheEvent.getNewValue(),cacheEvent.getType());
	
		log.info(result);
	}

}
