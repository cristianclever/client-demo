package com.verx.clientdemo.domain.dto;

import com.sun.istack.NotNull;

public class AddressDTO {

	@NotNull
	private Long postalCode;


	private String street;


	private String district;


	private String city;


	private String state;


	public Long getPostalCode() {
		return postalCode;
	}


	public void setPostalCode(Long postalCode) {
		this.postalCode = postalCode;
	}


	public String getStreet() {
		return street;
	}


	public void setStreet(String street) {
		this.street = street;
	}


	public String getDistrict() {
		return district;
	}


	public void setDistrict(String district) {
		this.district = district;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}
	
	

}
