package com.verx.clientdemo.domain.dto;

import java.util.List;

import org.springframework.web.bind.annotation.RequestParam;

public class PageableClientDTO {
	public List<ClientDTO> getListClientDTO() {
		return listClientDTO;
	}

	private List<ClientDTO> listClientDTO;

	/**
	 * Returns the page to be returned.
	 *
	 * @return the page to be returned.
	 */
	int pageNumber;
	int pageSize;
	long totalElements;
	int totalPages;
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public long getTotalElements() {
		return totalElements;
	}
	public void setTotalElements(long totalElements) {
		this.totalElements = totalElements;
	}
	public int getTotalPages() {
		return totalPages;
	}
	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}
	public void setListClientDTO(List<ClientDTO> listClientDTO) {
		this.listClientDTO = listClientDTO;
	}
	
	
	
	public PageableClientDTO(List<ClientDTO> listClientDTO, int pageNumber, int pageSize, long totalElements, int totalPages) {
		super();
		this.listClientDTO = listClientDTO;
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
		this.totalElements = totalElements;
		this.totalPages = totalPages;
	}


	
	
}
