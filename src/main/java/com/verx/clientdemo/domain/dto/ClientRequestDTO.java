package com.verx.clientdemo.domain.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

public class ClientRequestDTO {
	
	@NotBlank
	@Min(value = 11)
	private String cpf;

	@NotBlank
	@Min(value = 5)
	private String name;
	
	
	private Long postalCode;


	public String getCpf() {
		return cpf;
	}


	public void setCpf(String cpf) {
		this.cpf = cpf;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Long getPostalCode() {
		return postalCode;
	}


	public void setPostalCode(Long postalCode) {
		this.postalCode = postalCode;
	}
		
	

}
