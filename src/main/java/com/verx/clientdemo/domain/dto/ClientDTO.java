package com.verx.clientdemo.domain.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

public class ClientDTO {
	
	@NotBlank
	@Min(value = 11)
	private String cpf;

	@NotBlank
	@Min(value = 5)
	private String name;
	
	
	private AddressDTO address;
	

	public AddressDTO getAddress() {
		return address;
	}

	public void setAddress(AddressDTO address) {
		this.address = address;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	
	
	

}
