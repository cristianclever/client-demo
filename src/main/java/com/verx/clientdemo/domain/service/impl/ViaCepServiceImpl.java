package com.verx.clientdemo.domain.service.impl;

import java.text.MessageFormat;

import javax.cache.annotation.CacheKey;
import javax.cache.annotation.CacheResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.verx.clientdemo.domain.dto.ViacepDTO;
import com.verx.clientdemo.domain.exception.CepNotFoundException;
import com.verx.clientdemo.domain.handler.ExceptionHandler;
import com.verx.clientdemo.domain.service.ViaCepService;

@Service
public class ViaCepServiceImpl implements ViaCepService {
	
	final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(ViaCepServiceImpl.class);
	
	
	@Qualifier("rest-template-with-logger")
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${VIA_CEP_URL_TEMPLATE}")
	private String viaCepUrl;
	
	
	@Cacheable(value =  "cacheCEP")
	public ViacepDTO findByCep(Long cep) throws CepNotFoundException {
		
		log.info("Buscando CEP:" + cep);
		
		String strCep = String.valueOf(cep);
		
		if(strCep.length()!=8) {
			throw new CepNotFoundException(cep);
		}
		
		String url = MessageFormat.format(viaCepUrl, strCep);
		
		//StringBuilder url = new StringBuilder("https://viacep.com.br/ws/").append(cep).append("/json");
		
		HttpEntity<String> request = new HttpEntity<>("");
		ResponseEntity<ViacepDTO> response = restTemplate.exchange(url.toString(), HttpMethod.GET, request, ViacepDTO.class);

		if(response.getStatusCode()!=HttpStatus.OK) {
			throw new CepNotFoundException(cep);
		}
		return response.getBody();
		
	}
	
	

}
