package com.verx.clientdemo.domain.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.verx.clientdemo.domain.dto.AddressDTO;
import com.verx.clientdemo.domain.dto.ClientDTO;
import com.verx.clientdemo.domain.dto.ClientRequestDTO;
import com.verx.clientdemo.domain.dto.PageableClientDTO;
import com.verx.clientdemo.domain.dto.ViacepDTO;
import com.verx.clientdemo.domain.entity.Address;
import com.verx.clientdemo.domain.entity.Client;
import com.verx.clientdemo.domain.exception.CepNotFoundException;
import com.verx.clientdemo.domain.repository.AddressRepository;
import com.verx.clientdemo.domain.repository.ClientRepository;
import com.verx.clientdemo.domain.service.PersistenceService;

@Service
public class PersistenceServiceImpl implements PersistenceService {

	@Autowired
	private ClientRepository clientRepository;

	@Autowired
	private AddressRepository addressRepository;

	@Autowired
	private ViaCepServiceImpl viacepService;

	public ClientDTO findByID(String cpf) throws EmptyResultDataAccessException {
		Client c = clientRepository.findById(cpf).orElseThrow(() -> new EmptyResultDataAccessException(1));
		return toDto(c);
	}

	public PageableClientDTO findAll(Pageable p) throws EmptyResultDataAccessException {
		Page<Client> page = clientRepository.findAll(p);
		List<ClientDTO> listClientDTO = page.get().map(e -> toDto(e)).collect(Collectors.toList());
		
		int pageSize = p.isUnpaged()?0:p.getPageSize();
		int totalPages = p.isUnpaged()?0:page.getTotalPages();
		long totalElements = page.getTotalElements();
		int pageNumber = p.isUnpaged()?0:p.getPageNumber();
		 
		 
		return new 	PageableClientDTO(listClientDTO,  pageNumber,  pageSize,  totalElements,totalPages);

	}

	public void deleteByID(String cpf) {
		clientRepository.deleteById(cpf);
	}

	public ClientDTO insert(ClientRequestDTO dto) throws CepNotFoundException {

		ClientDTO inserted = null;
		Client clientpersisted = clientRepository.findById(dto.getCpf()).orElse(null);

		if (clientpersisted == null) {

			Client client2Persist = toEntity(dto);

			if (client2Persist.getAddress() != null) {



					// precisa obter o cep.Utiliza o valor do cache caso exista
					ViacepDTO cepDTO = viacepService.findByCep(client2Persist.getAddress().getPostalCode());

					Address address = new Address();
					address.setPostalCode(client2Persist.getAddress().getPostalCode());
					address.setCity(cepDTO.getLocalidade());
					address.setDistrict(cepDTO.getBairro());
					address.setStreet(cepDTO.getLogradouro());
					address.setState(cepDTO.getUf());

					addressRepository.save(address);
					client2Persist.setAddress(address);
				

			}

			clientRepository.save(client2Persist);
			inserted = toDto(client2Persist);

		}

		return inserted;
	}

	public ClientDTO update(ClientRequestDTO dto) {

		ClientDTO result = null;
		Client c = clientRepository.findById(dto.getCpf()).orElse(null);
		if (c != null) {
			c.setCpf(dto.getCpf());
			c.setName(dto.getName());
			Client updatedClient = clientRepository.save(c);
			result = toDto(updatedClient);
		}
		return result;
	}

	private Client toEntity(ClientRequestDTO dto) {
		Client c = new Client();
		c.setCpf(dto.getCpf());
		c.setName(dto.getName());

		if (dto.getPostalCode() != null) {
			Address a = new Address();
			a.setPostalCode(dto.getPostalCode());
			c.setAddress(a);
		}

		return c;
	}

	private ClientDTO toDto(Client e) {
		ClientDTO dto = new ClientDTO();
		dto.setCpf(e.getCpf());
		dto.setName(e.getName());

		if (e.getAddress() != null) {
			AddressDTO address = new AddressDTO();

			address.setPostalCode(e.getAddress().getPostalCode());
			address.setCity(e.getAddress().getCity());
			address.setDistrict(e.getAddress().getDistrict());
			address.setStreet(e.getAddress().getStreet());
			address.setState(e.getAddress().getState());

			dto.setAddress(address);
		}

		return dto;
	}
}
