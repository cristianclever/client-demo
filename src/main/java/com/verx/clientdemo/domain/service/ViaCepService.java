package com.verx.clientdemo.domain.service;

import javax.cache.annotation.CacheKey;

import com.verx.clientdemo.domain.dto.ViacepDTO;
import com.verx.clientdemo.domain.exception.CepNotFoundException;

public interface ViaCepService {
	ViacepDTO findByCep(Long cep) throws CepNotFoundException;
}