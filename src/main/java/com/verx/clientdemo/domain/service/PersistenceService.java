package com.verx.clientdemo.domain.service;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Pageable;

import com.verx.clientdemo.domain.dto.ClientDTO;
import com.verx.clientdemo.domain.dto.ClientRequestDTO;
import com.verx.clientdemo.domain.dto.PageableClientDTO;

public interface PersistenceService {

	ClientDTO findByID(String cpf) throws EmptyResultDataAccessException;

	PageableClientDTO findAll(Pageable p);

		
	void deleteByID(String cpf);

	ClientDTO insert(ClientRequestDTO dto);

	ClientDTO update(ClientRequestDTO dto);

}